package com.demoproject.employeemanagement.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.demoproject.employeemanagement.entity.Tasks;

public interface TaskRepository extends MongoRepository<Tasks, Integer> {
	
	List<Tasks> getTasksByEmployeeId(@Param("employeeId") int employeeId);

}
