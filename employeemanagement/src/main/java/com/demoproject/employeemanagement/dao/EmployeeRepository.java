package com.demoproject.employeemanagement.dao;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.demoproject.employeemanagement.entity.Employee;

@CrossOrigin
public interface EmployeeRepository extends MongoRepository<Employee, Integer> {

}
