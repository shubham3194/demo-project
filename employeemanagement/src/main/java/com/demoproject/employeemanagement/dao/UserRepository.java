package com.demoproject.employeemanagement.dao;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.demoproject.employeemanagement.entity.User;

public interface UserRepository extends MongoRepository<User, Integer> {

	User getUserByUsername(@Param("username") String username);
	
}
