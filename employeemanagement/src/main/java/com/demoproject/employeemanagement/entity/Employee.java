package com.demoproject.employeemanagement.entity;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employee")
public class Employee {
	
	@Id
	private int employeeId;
	
	
	private String studentName;
	
	
	private String projectName;
	
	
	
	

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}



	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", studentName=" + studentName + ", projectName=" + projectName
				+ "]";
	}
	

}
