package com.demoproject.employeemanagement.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.employeemanagement.dao.UserRepository;
import com.demoproject.employeemanagement.entity.User;

@RestController
@CrossOrigin
public class LoginController {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/user")
	public List<User> getUser(){
		return userRepository.findAll();
	}
	
	@GetMapping("/user/{id}")
	public Optional<User> getUserById(@PathVariable int id) {
		return userRepository.findById(id);
	}
	
	@GetMapping("/user/get/{username}")
	public User getUserByUsername(@PathVariable String username) {
		return userRepository.getUserByUsername(username);
	}

	@PostMapping("/user")
	public void saveUser(@RequestBody User user) {
		userRepository.save(user);
	}
	
	@PutMapping("/user/{id}")
	public void updateUser(@PathVariable int id,@RequestBody User user) {
		userRepository.save(user);
	}
	
	@DeleteMapping("/user/{id}")
	public void deleteUser(@PathVariable int id) {
		userRepository.deleteById(id);
	}
	
}
