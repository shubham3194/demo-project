package com.demoproject.employeemanagement.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demoproject.employeemanagement.dao.TaskRepository;
import com.demoproject.employeemanagement.entity.Tasks;

@RestController
@CrossOrigin
public class TaskController {

	@Autowired
	private TaskRepository taskRepository;
	
	@GetMapping("/tasks")
	public List<Tasks> gettasks(){
		return taskRepository.findAll();
	}
	
	@GetMapping("/tasks/{id}")
	public Optional<Tasks> gettaskById(@PathVariable int id) {
		return taskRepository.findById(id);
	}
	
	@GetMapping("/tasks/get/{id}")
	public List<Tasks> gettasks(@PathVariable int id){
		return taskRepository.getTasksByEmployeeId(id);
	}
	
	@PostMapping("/tasks")
	public void savetask(@RequestBody Tasks task) {
		taskRepository.save(task);
	}
	
	@PutMapping("/tasks/{id}")
	public void updatetask(@PathVariable int id,@RequestBody Tasks task) {
		taskRepository.save(task);
	}
	
	@DeleteMapping("/tasks/{id}")
	public void deletetask(@PathVariable int id) {
		taskRepository.deleteById(id);
	}
	
}
